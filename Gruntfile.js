module.exports = function (grunt) {
    require('time-grunt')(grunt);
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin'
    });
    grunt.initConfig({
        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'css',
                    src: ['*.scss'],
                    dest: 'css',
                    ext: '.css'
                }]
            }
        },
        // estamos configurando la herramienta de sass (sass:)

        // para que genere la versión de distribución buscando en todos los archivos(dist:)

        // // dentro de la carpeta css que termina con extensión scs. Los mande dentro del destino, de la carpeta de distribución a una carpeta que sea css y le pongo la extensión .css.

        copy: {
            html: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: './',
                    src: ['*.html'], // coge los archivos html y los manda a la carpeta dist
                    dest: 'dist/',
                }]
            },
        },

        clean: {
            build: {
                drc: ['dist/'] // elimina la carpeta 
            }
        },

        cssmin: {// css
            dist: {}
        },


        uglify: { //js
            dist: {}
        },

        filerev: {
            options: {
                encoding: 'utf8',
                algorithm: 'md5',
                length: 20
            },
            release: {
                // filerev:release hashes(md5) all assets (images, js and css)
                //in dist directory
                files: [{
                    src: [
                        'dist/js/*.js',
                        'dist/css/*.css',

                    ]
                }]
            }
        },

        concat:{
            options:{
                separator: ';'},
                dist: {}
            },
        
useminPrepare:{
    foo:{
        dest:'dist',
        src:['index.html', 'about.html', 'precios.html', 'contacto.html']
    },
    options:{
        flow:{
            steps:{
                css:['cssmin'], //les aplica cssmin y uglify para guardarlos en la carpeta dist
                js:['uglify']
            },
            post:{
                css:[{
                    name: 'cssmin',
                    createConfig:function(context,block){
                        var generated = context.options.generated;
                        generated.options ={
                            keepSpecialComments:0,rebase:false
                        }
                    }
                }]
            }
        }
    }
},
usemin: {

    html:['dist/index.html', 'dist/about.html', 'dist/precios.html', 'dist/contacto.html'],
    options: {
        assetsDir: [ 'dist', 'dist/css', 'dist/js']
    }
},// le diremos que os html tomen los assets de los dist

         watch: {
            files: ['css/*.scss'],//observe los archivosde lacarpeta css con .scss
            tasks: ['css']
            //cuando tengamos un cambio en la carpeta css se va aejecutar la tarea 'css' que la definimos abajo que basicamente es sass (cambiar de scss a css)

            // watch es una tarea para la consola "grunt watch" poner en la cons de git 
        },

        browserSync: { // otra tarea para la consola de git 
            dev: {
                bsFiles: {
                    src: ['css/*.scss', '*.html', 'js/*.js']//caretas qe va a vigilar 
                },
                options: {
                    watchTask: true, //si qeremos la escucha constante 
                    server: {
                        baseDir: './' //directorio base para nuestro servidor
                    }
                }
            }
        },
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: './',
                    src: ['images/*.{png,jpg,gif,jpeg}'],//busca en la carpeta images 
                    dest: 'dist/' //con destino a la carpeta dist
                }]
            }
        },




    });

   
    grunt.registerTask('css', ['sass']);// registramos la tarea en la consola escribimos "grunt css"
    grunt.registerTask('default', ['browserSync', 'watch']); // la palabra "default" es la que itroducimos en la consola que se guardaria como un comando que ejecutaria las tareas 'browserSync' y 'watch'
    grunt.registerTask('img:compress', ['imagemin']);
    grunt.registerTask('build', ['clean','copy', 'imagemin','useminPrepare', 'concat', 'cssmin','uglify', 'filerev', 'usemin']);
}

